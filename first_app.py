from flask import Flask, render_template, request, redirect, url_for

from flask_login import LoginManager, current_user, login_user

from user_model import User
from login_form import LoginForm


app = Flask(__name__)
app.config['SECRET_KEY'] = 'you-will-never-guess'
app.config['DEBUG'] = True
login = LoginManager(app)

@login.user_loader
def load_user(user_id):
    if user_id == 1:
        user = User(1, 'username', 'Name')
        user.is_authenticated = True
        return user

@app.route('/login', methods=['GET', 'POST'])
def login():
    method = request.method
    if current_user.is_authenticated:
        return redirect(url_for('hello_world'))
    form = LoginForm()
    if form.validate_on_submit():
        if form.username.data == 'username':
            user = User(1, 'username', 'Name')
            user.password_hash = 'pbkdf2:sha256:260000$VtLwPk2OwzaeQZOH$7d38597df97530cce0207a396028a6dee1106e34d0248f24b6e4a67e16e49969'
            if user is None or not user.check_password(form.password.data):
                return redirect(url_for('login'))
            login_user(user, remember=form.remember_me.data)
            return redirect(f'/user/{user.id}')
    return render_template('login.html', title='Sign In', form=form)

@app.route('/')
def hello_world():
    return '<H1>Hello, World!</H1><br/><div><b>Bold text</b></div>'


@app.route('/help')
def help_route():
    return '<h2>Допомоги немає, але ви тримайтесь там.</h2>'


@app.route('/user/<int:user_id>')
def user_route(user_id):
    return f'<h2>Привіт {user_id} user.</h2>'


@app.route('/user/<user_name>')
def user_name_route(user_name):
    return f'<h2>Привіт {user_name}!</h2>'


@app.route('/users')
def users_route():
    return render_template('index.html', users=[i for i in range(10)])


@app.route('/users/<name>')
def users_name_route(name):
    return render_template('index.html', name=name, users=[i for i in range(10)])


@app.route('/users/login')
def users_login():
    form = LoginForm()
    return render_template('login.html', title='Sign In', form=form)

if __name__ == '__main__':
    app.run()
